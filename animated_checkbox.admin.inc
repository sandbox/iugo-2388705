<?php

/**
 * config form.
 */
function animated_checkbox_config_form($form, &$form_state) {
  $form = array();
  
  $form['theme_forms_list_type'] = array(
    '#type' => 'radios',
    '#title' => t('Theme checkboxes/radios in specific forms'),
    '#default_value' => variable_get('theme_forms_list_type',1),
    '#options' => array(
    	'1' => t('All forms except those listed'),
    	'2' => t('Only the listed forms'),
    ),
  );
  
  $form['theme_forms_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Forms list'),
    '#default_value' => variable_get('theme_forms_list'),
    '#description' => t('Specify form IDs separated by a comma.'),
  );
  
    $form['anch_checkboxes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Checkboxes'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['anch_radios'] = array(
    '#type' => 'fieldset',
    '#title' => t('Radios'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
   $form['anch_radios']['anch_radios_innercolor'] = array(
'#type' =>'textfield', 
'#title' => t('Radios fill color'),
'#default_value' => variable_get('anch_radios_innercolor','#c0c0c0'),
'#maxlength' => 7, 
'#size'=> 7,
);
$form['anch_radios']['anch_radios_linewidth'] = array(
       '#type' => 'select',
       '#title' => t('Inner line width'),
       '#options' => array(
         drupal_map_assoc(array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)),
       ),
       '#default_value' => variable_get('anch_radios_linewidth',2),
);

$form['anch_radios']['anch_radios_borderwidth'] = array(
'#type' =>'textfield', 
'#title' => t('Border width'),
'#default_value' => variable_get('anch_radios_borderwidth','0.15em'),
'#maxlength' => 10, 
'#size'=> 10,
'#description' => t('Must be valid css value, e.g.: 0.15em.'),
);

  $form['anch_radios']['anch_radios_outhcolor'] = array(
'#type' =>'textfield', 
'#title' => t('Radios outh color'),
'#default_value' => variable_get('anch_radios_outhcolor','#c0c0c0'),
'#maxlength' => 7, 
'#size'=> 7,
);

$form['anch_radios']['anch_radios_animation'] = array(
       '#type' => 'select',
       '#title' => t('Animation type'),
       '#options' => array('fill'=>t('Fill'),'circle'=>t('Circle'),'swirl'=>t('Swirl')),
       '#default_value' => variable_get('anch_radios_animation','fill'),
);

$form['anch_radios']['anch_radios_animspeed'] = array(
'#type' =>'textfield', 
'#title' => t('Animation speed'),
'#default_value' => variable_get('anch_radios_animspeed',500),
'#maxlength' => 7, 
'#size'=> 7,
'#description' => t('In milliseconds.'),
);

$form['anch_checkboxes']['anch_checkboxes_innercolor'] = array(
'#type' =>'textfield', 
'#title' => t('Checkboxes fill color'),
'#default_value' => variable_get('anch_checkboxes_innercolor','#c0c0c0'),
'#maxlength' => 7, 
'#size'=> 7,
);

$form['anch_checkboxes']['anch_checkboxes_linewidth'] = array(
       '#type' => 'select',
       '#title' => t('Inner line width'),
       '#options' => array(
         drupal_map_assoc(array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)),
       ),
       '#default_value' => variable_get('anch_checkboxes_linewidth',2),
);

$form['anch_checkboxes']['anch_checkboxes_borderwidth'] = array(
'#type' =>'textfield', 
'#title' => t('Border width'),
'#default_value' => variable_get('anch_checkboxes_borderwidth','0.15em'),
'#maxlength' => 10, 
'#size'=> 10,
'#description' => t('Must be valid css value, e.g.: 0.15em.'),
);

$form['anch_checkboxes']['anch_checkboxes_outhcolor'] = array(
'#type' =>'textfield', 
'#title' => t('Checkboxes outh color'),
'#default_value' => variable_get('anch_checkboxes_outhcolor','#c0c0c0'),
'#maxlength' => 7, 
'#size'=> 7,
);

$form['anch_checkboxes']['anch_checkboxes_animation'] = array(
       '#type' => 'select',
       '#title' => t('Animation type'),
       '#options' => array('cross'=>t('Cross'),'checkmark'=>t('Checkmark'),'circle'=>t('Circle'),'boxfill'=>t('Boxfill'),'diagonal'=>t('Diagonal')),
       '#default_value' => variable_get('anch_checkboxes_animation','checkmark'),
);

$form['anch_checkboxes']['anch_checkboxes_animspeed'] = array(
'#type' =>'textfield', 
'#title' => t('Animation speed'),
'#default_value' => variable_get('anch_checkboxes_animspeed',500),
'#maxlength' => 7, 
'#size'=> 7,
'#description' => t('In milliseconds.'),
);
 
  return system_settings_form($form);
}
?>